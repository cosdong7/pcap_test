#include <pcap.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <stdint.h>
#include "packet_headers.h"

void print_mac(uint8_t *mac){
    printf("%02x:%02x:%02x:%02x:%02x:%02x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void print_ip(uint8_t *ip){
    printf("%d.%d.%d.%d\n", ip[0], ip[1], ip[2], ip[3]);
}

void print_port(uint16_t port){
    printf("%d\n", port);
}
void usage() {
  printf("syntax: pcap_test <interface>\n");
  printf("sample: pcap_test wlan0\n");
}

int main(int argc, char* argv[]) {
  printf("Ethernet-IPv4-TCP capturing program. v1.0\nmade by cosdong7\n");

  if (argc != 2) {
    usage();
    return -1;
  }

  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

  printf("Capture start! \n\n");
  int cnt = 0;
  while (true) {
    struct pcap_pkthdr* header;
    const u_char* packet;
    int res = pcap_next_ex(handle, &header, &packet);
    if (res == 0) continue;
    if (res == -1 || res == -2) break;
    printf("\n\n\n");
    printf("%u bytes captured\n", header->caplen);

    //ether_header
    struct Ether_h* ether_h = (struct Ether_h*)packet;
    printf("Dst MAC: ");
    print_mac(ether_h->dmac);
    printf("Src MAC: ");
    print_mac(ether_h->smac);
    if (ntohs(ether_h->ether_type)!=0x0800){
        printf("Ether type is not IP!\n");
        printf("Ether type number: %x\n\n\n", ntohs(ether_h->ether_type));
        continue;
    }

    //ip_header
    struct Ip_h* ip_h = (struct Ip_h*)ether_h->payload;
    int version = (ip_h->version_ihl & 0xf0)>>4;
    if (version != 4){
        printf("IP version is not 4!\n");
        printf("IP version: %d\n", version);
        continue;
    }
    printf("Src ip: ");
    print_ip(ip_h->sip);
    printf("Dst ip: ");
    print_ip(ip_h->dip);

    if (ip_h->protocol != 6){
        printf("Protocol is not 6(tcp)!\n");
        printf("Protocol number: %x\n", ip_h->protocol);
        continue;
    }
    uint ihl = 4 * (ip_h->version_ihl & 0x0f);


    //tcp_header
    struct Tcp_h* tcp_h = (struct Tcp_h*)(ip_h->payload+ihl-20);
    printf("Src port: ");
    print_port(ntohs(tcp_h->sport));
    printf("Dst port: ");
    print_port(ntohs(tcp_h->dport));
    uint offset = 4 * ((tcp_h->offset_resvd & 0xf0) >> 4);
    printf("tcp data: ");
    uint data_len = header->len-14-ihl-offset;
    for(uint i = 0; i < (data_len>10 ? 10:data_len); i++){
        printf("%02x ", *(tcp_h->payload+offset-20+i));
    }
    printf("\tdata len: %d\n", data_len);
    cnt++;
    printf("%d \'eth-ip-tcp\' packets captured", cnt);
  }

  pcap_close(handle);
  return 0;
}
