#pragma once
#include <stdint.h>
#include <pcap.h>

struct Ether_h{
    uint8_t dmac[6];
    uint8_t smac[6];
    uint16_t ether_type;
    uint8_t payload[1];
};

struct Ip_h{
    uint8_t version_ihl;
    uint8_t tos;
    uint16_t totl;
    uint16_t id;
    uint16_t flag_foffset;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t hchecksum;
    uint8_t sip[4];
    uint8_t dip[4];
    uint8_t payload[1];
};

struct Tcp_h{
    uint16_t sport;
    uint16_t dport;
    uint32_t seqnum;
    uint32_t acknum;
    uint8_t offset_resvd;
    uint8_t flag;
    uint16_t window;
    uint16_t checksum;
    uint16_t urgpointer;
    uint8_t payload[1];
};
